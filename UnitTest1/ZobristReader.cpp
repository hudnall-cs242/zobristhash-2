#include "stdafx.h"
#include <string>
using namespace std;

#include "ZobristReader.h"

ZobristReader::ZobristReader(const string& filename, int bufferSize) : 
	buffer_(new char[bufferSize]), bufferSize_(bufferSize), nBytesInBuffer_(0),
	nextByteIndex_(0), stream_(filename, std::fstream::binary), eof_(false)
{
}

bool ZobristReader::nextByte(char& nextChar)
{
	bool returnValue = true;
	int nBytesRead = 0;
	int offset = 0;

	if (nBytesInBuffer_ == 0)
	{
		stream_.read(buffer_, bufferSize_);

		if (stream_)
		{
			nBytesInBuffer_ = bufferSize_;
			nextByteIndex_ = 0;
		}
		else
		{
			if (!eof_)
			{
				nBytesInBuffer_ = stream_.gcount();
			    nextByteIndex_ = 0;
				eof_ = true;
			}
			else
			{
				returnValue = false;
			}
		}
	}
	
	if (returnValue)
	{
		nextChar = buffer_[nextByteIndex_++];
		--nBytesInBuffer_;
	}
	return returnValue;
}


ZobristReader::~ZobristReader()
{
	delete[] buffer_;
}
