#pragma once
#include <iostream>
#include <fstream>
#include <string>
class ZobristReader
{
public:
	enum { DEFAULT_BUFFER_SIZE = 16 * 1024 };
	ZobristReader(const std::string& filename, int bufferSize = DEFAULT_BUFFER_SIZE);
	bool nextByte(char& nextChar);
	virtual ~ZobristReader();
private:
	char* buffer_;
	int bufferSize_;
	int nBytesInBuffer_;
	int nextByteIndex_;
	bool eof_;
	std::ifstream stream_;
};

