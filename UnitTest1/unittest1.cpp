#include "stdafx.h"
#include "CppUnitTest.h"
#include "../zobristLib/ZobristChessHash.h"
#include "../zobristLib/GameReader.h"
#include "ZobristReader.h"
#include <fstream>
using namespace std;
using namespace cs242;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{
	TEST_CLASS(UnitTest1)
	{
	public:

		TEST_METHOD(testGetPieceIndex)
		{
			ZobristChessHash zobrist;
			Assert::AreEqual((int)ZobristChessHash::WHITE_PAWN,
				(int)zobrist.getPieceIndex('P', true));
			Assert::AreEqual((int)ZobristChessHash::WHITE_KNIGHT,
				(int)zobrist.getPieceIndex('N', true));
			Assert::AreEqual((int)ZobristChessHash::WHITE_BISHOP,
				(int)zobrist.getPieceIndex('B', true));
			Assert::AreEqual((int)ZobristChessHash::WHITE_ROOK,
				(int)zobrist.getPieceIndex('R', true));
			Assert::AreEqual((int)ZobristChessHash::WHITE_QUEEN,
				(int)zobrist.getPieceIndex('Q', true));
			Assert::AreEqual((int)ZobristChessHash::WHITE_KING,
				(int)zobrist.getPieceIndex('K', true));

			Assert::AreEqual((int)ZobristChessHash::BLACK_PAWN,
				(int)zobrist.getPieceIndex('P', false));
			Assert::AreEqual((int)ZobristChessHash::BLACK_KNIGHT,
				(int)zobrist.getPieceIndex('N', false));
			Assert::AreEqual((int)ZobristChessHash::BLACK_BISHOP,
				(int)zobrist.getPieceIndex('B', false));
			Assert::AreEqual((int)ZobristChessHash::BLACK_ROOK,
				(int)zobrist.getPieceIndex('R', false));
			Assert::AreEqual((int)ZobristChessHash::BLACK_QUEEN,
				(int)zobrist.getPieceIndex('Q', false));
			Assert::AreEqual((int)ZobristChessHash::BLACK_KING,
				(int)zobrist.getPieceIndex('K', false));
		}

		TEST_METHOD(testGetSquareIndex)
		{
			Assert::AreEqual(0, ZobristChessHash::getSquareIndex("a1"));
			Assert::AreEqual(1, ZobristChessHash::getSquareIndex("a2"));
			Assert::AreEqual(2, ZobristChessHash::getSquareIndex("a3"));
			Assert::AreEqual(3, ZobristChessHash::getSquareIndex("a4"));
			Assert::AreEqual(4, ZobristChessHash::getSquareIndex("a5"));
			Assert::AreEqual(5, ZobristChessHash::getSquareIndex("a6"));
			Assert::AreEqual(6, ZobristChessHash::getSquareIndex("a7"));
			Assert::AreEqual(7, ZobristChessHash::getSquareIndex("a8"));

			Assert::AreEqual(15, ZobristChessHash::getSquareIndex("b8"));

			Assert::AreEqual(23, ZobristChessHash::getSquareIndex("c8"));

			Assert::AreEqual(31, ZobristChessHash::getSquareIndex("d8"));

			Assert::AreEqual(39, ZobristChessHash::getSquareIndex("e8"));

			Assert::AreEqual(47, ZobristChessHash::getSquareIndex("f8"));

			Assert::AreEqual(55, ZobristChessHash::getSquareIndex("g8"));

			Assert::AreEqual(56, ZobristChessHash::getSquareIndex("h1"));
			Assert::AreEqual(57, ZobristChessHash::getSquareIndex("h2"));
			Assert::AreEqual(58, ZobristChessHash::getSquareIndex("h3"));
			Assert::AreEqual(59, ZobristChessHash::getSquareIndex("h4"));
			Assert::AreEqual(60, ZobristChessHash::getSquareIndex("h5"));
			Assert::AreEqual(61, ZobristChessHash::getSquareIndex("h6"));
			Assert::AreEqual(62, ZobristChessHash::getSquareIndex("h7"));
			Assert::AreEqual(63, ZobristChessHash::getSquareIndex("h8"));
		}

		TEST_METHOD(placePiece)
		{
			ZobristChessHash zobrist;
			unsigned long long oldHash = zobrist.getHash();
			unsigned long long newHash = zobrist.placePiece('N', true, "f3");
			Assert::IsTrue(oldHash != newHash);

			unsigned long long restoreHash = zobrist.placePiece(' ', true, "f3");
			Assert::IsTrue(restoreHash == oldHash);
		}

		TEST_METHOD(clearBoard)
		{
			ZobristChessHash zobrist;

			unsigned long long hash = zobrist.getHash();
			zobrist.clearBoard();
			Assert::IsTrue(hash == zobrist.getHash());
		}

		TEST_METHOD(setupBoard)
		{
			ZobristChessHash zobrist;

			unsigned long long setupHash = zobrist.setupPieces();
			unsigned long long secondHash = zobrist.setupPieces();
			Assert::IsTrue(setupHash == secondHash);
		}

		TEST_METHOD(moveTest)
		{
			ZobristChessHash zobrist;
			zobrist.setupPieces();

			ZobristChessHash moveOrder;
			moveOrder.setupPieces();

			Assert::IsTrue(zobrist.getHash() == moveOrder.getHash());
			zobrist.movePiece('P', true, "e2", "e4");
			zobrist.movePiece('P', false, "e7", "e5");
			zobrist.movePiece('N', true, "g1", "f3");
			zobrist.movePiece('N', false, "b8", "c6");

			
			
			moveOrder.movePiece('N', true, "g1", "f3");
			moveOrder.movePiece('N', false, "b8", "c6");
			moveOrder.movePiece('P', true, "e2", "e4");
			moveOrder.movePiece('P', false, "e7", "e5");
			Assert::IsTrue(zobrist.getHash() == moveOrder.getHash());

			
		}

		TEST_METHOD(moveTestWithSquareNumbers)
		{
			ZobristChessHash zobrist;
			zobrist.setupPieces();

			ZobristChessHash justSquares;
			justSquares.setupPieces();
			Assert::IsTrue(zobrist.getHash() == justSquares.getHash());
			// get the square numbers for the move
			int sourceSquare = GameReader::squareNameToInt("e2");
			int destinationSquare = GameReader::squareNameToInt("e4");
			justSquares.movePiece(sourceSquare, destinationSquare);
			zobrist.movePiece('P', true, "e2", "e4");
			Assert::IsTrue(zobrist.getHash() == justSquares.getHash());


			sourceSquare = GameReader::squareNameToInt("e7");
			destinationSquare = GameReader::squareNameToInt("e5");
			justSquares.movePiece(sourceSquare, destinationSquare);
			zobrist.movePiece('P', false, "e7", "e5");
			Assert::IsTrue(zobrist.getHash() == justSquares.getHash());

			sourceSquare = GameReader::squareNameToInt("g1");
			destinationSquare = GameReader::squareNameToInt("f3");
			justSquares.movePiece(sourceSquare, destinationSquare);
			zobrist.movePiece('N', true, "g1", "f3");
			Assert::IsTrue(zobrist.getHash() == justSquares.getHash());

			sourceSquare = GameReader::squareNameToInt("b8");
			destinationSquare = GameReader::squareNameToInt("c6");
			justSquares.movePiece(sourceSquare, destinationSquare);
			zobrist.movePiece('N', false, "b8", "c6");
			Assert::IsTrue(zobrist.getHash() == justSquares.getHash());

			ZobristChessHash moveOrder;
			moveOrder.setupPieces();
			moveOrder.movePiece('N', true, "g1", "f3");
			moveOrder.movePiece('N', false, "b8", "c6");
			moveOrder.movePiece('P', true, "e2", "e4");
			moveOrder.movePiece('P', false, "e7", "e5");

			Assert::IsTrue(zobrist.getHash() == moveOrder.getHash());
			Assert::IsTrue(justSquares.getHash() == moveOrder.getHash());
			//Assert::AreEqual(string("abc"), moveOrder.getPositionInFEN());
		}



		TEST_METHOD(gameReaderSimpleTest)
		{
			GameReader gameReader("cs242-oneGame.pgn");
			int sourceSquare;
			int destinationSquare;
			GameReader::GameResult gameProgress;

			bool isWhitesMove = true;
			int plyNumber = 0;
			int castles = 0;

			do
			{
				Assert::IsTrue(isWhitesMove == gameReader.isWhitesMove());

				gameReader.read(sourceSquare, destinationSquare,
					gameProgress);

				if (!gameReader.isCastling())
				{
					isWhitesMove = !isWhitesMove;
					if (gameProgress == GameReader::GameResult::InProgress)
						++plyNumber;
				}
				else
				{
					++castles;
				}

			} while (gameProgress == GameReader::GameResult::InProgress);

			Assert::AreEqual((int)GameReader::GameResult::WhiteWins,
				(int)gameProgress);

			Assert::AreEqual(2, castles);
			Assert::AreEqual(41, plyNumber);
		}

		TEST_METHOD(gameReaderNoMoves)
		{
			GameReader gameReader("cs242-noMoves.pgn");
			int sourceSquare;
			int destinationSquare;
			GameReader::GameResult gameProgress;

			bool isWhitesMove = true;
			int plyNumber = 0;
			int castles = 0;

			do
			{
				Assert::IsTrue(isWhitesMove == gameReader.isWhitesMove());

				gameReader.read(sourceSquare, destinationSquare,
					gameProgress);

				if (!gameReader.isCastling())
				{
					isWhitesMove = !isWhitesMove;
					if (gameProgress == GameReader::GameResult::InProgress)
						++plyNumber;
				}
				else
				{
					++castles;
				}

			} while (gameProgress == GameReader::GameResult::InProgress);

			Assert::AreEqual((int)GameReader::GameResult::BlackWins,
				(int)gameProgress);

			Assert::AreEqual(0, castles);
			Assert::AreEqual(0, plyNumber);
		}

		TEST_METHOD(gameReaderShortDraw)
		{
			GameReader gameReader("cs242-shortDraw.pgn");
			int sourceSquare;
			int destinationSquare;
			GameReader::GameResult gameProgress;

			bool isWhitesMove = true;
			int plyNumber = 0;
			int castles = 0;

			do
			{
				Assert::IsTrue(isWhitesMove == gameReader.isWhitesMove());

				gameReader.read(sourceSquare, destinationSquare,
					gameProgress);

				if (!gameReader.isCastling())
				{
					isWhitesMove = !isWhitesMove;
					if (gameProgress == GameReader::GameResult::InProgress)
						++plyNumber;
				}
				else
				{
					++castles;
				}

			} while (gameProgress == GameReader::GameResult::InProgress);

			Assert::AreEqual((int)GameReader::GameResult::Draw,
				(int)gameProgress);

			Assert::AreEqual(0, castles);
			Assert::AreEqual(2, plyNumber);
		}

		TEST_METHOD(gameReaderConvert10Games)
		{
			GameReader gameReader("cs242-10Games.pgn");
			int gamesRead = gameReader.convertToBinary("cs242-10Games.bin");
			Assert::AreEqual(10, gamesRead);
		}

		TEST_METHOD(gameReaderConvertAllGames)
		{
			GameReader gameReader("cs242-allGames.pgn");
			int gamesRead = gameReader.convertToBinary("cs242-allGames.bin");
			Assert::AreEqual(1363050, gamesRead);
		}

		
		TEST_METHOD(readBinary)
		{
			ifstream binGames("cs242-allGames.bin", std::fstream::binary);
			const int bytesPerRead = 16 * 1024;
			char buffer[bytesPerRead];
			int nBytesRead = 0;
			while (binGames)
			{
				binGames.read(buffer, bytesPerRead);
				if (binGames)
					nBytesRead += bytesPerRead;
				else
					nBytesRead += binGames.gcount();

				int nJustRead = (binGames) ? bytesPerRead : binGames.gcount();
				for (int i = 0; i < nJustRead; i++)
				{
					Assert::IsTrue(buffer[i] < 64);
				}
			}
			binGames.close();
			Assert::AreEqual(188433594, nBytesRead);
		}
		
		
		TEST_METHOD(gameReaderReadElevenGames)
		{
			GameReader gameReader("cs242-10Games.pgn");
			int sourceSquare;
			int destinationSquare;
			GameReader::GameResult gameProgress;

			bool isWhitesMove = true;
			int plyNumber = 0;
			int castles = 0;
			int nGames = 0;

			bool goodRead = true;
			while (goodRead)
			{
				do
				{
					Assert::IsTrue(isWhitesMove == gameReader.isWhitesMove());

					if (goodRead = gameReader.read(sourceSquare, destinationSquare,
						gameProgress))
					{


						if (!gameReader.isCastling())
						{
							isWhitesMove = !isWhitesMove;
							if (gameProgress == GameReader::GameResult::InProgress)
								++plyNumber;
						}
						else
						{
							++castles;
						}
					}
				} while (goodRead && gameProgress == GameReader::GameResult::InProgress);
				++nGames;
			}

			Assert::IsTrue(2 >= castles);
			Assert::AreEqual(11, nGames);
		}

		TEST_METHOD(testSimpleBinaryRead)
		{
			ifstream binGames("cs242-allGames.bin", std::fstream::binary);
			const int bytesPerRead = 16 * 1024;
			char buffer[bytesPerRead];
			int nBytesRead = 0;
			int nGames = 0;
			ZobristChessHash hash;
			hash.setupPieces();

			int offset = 0;
			while (binGames) // 362
			{
				binGames.read(buffer + offset, bytesPerRead - offset);
				if (binGames)
					nBytesRead = bytesPerRead - offset;
				else
					nBytesRead = binGames.gcount();

				int bytesProcessed;
				for (bytesProcessed = 0; bytesProcessed < nBytesRead - 1; bytesProcessed++)
				{
					if (buffer[bytesProcessed] < 0)
					{
						++nGames;
					}
					else
					{
						Assert::IsTrue(buffer[bytesProcessed] < 64);
						Assert::IsTrue(buffer[bytesProcessed + 1] < 64);
					}
				}

				if (bytesProcessed < bytesPerRead)
				{
					buffer[0] = buffer[bytesProcessed];
					offset = 1;
				}
				else
				{
					offset = 0;
				}
			}
			binGames.close();
			Assert::AreEqual(1362978, nGames);
		}

		TEST_METHOD(testBinaryFile)
		{
			const unsigned long long TABLE_SIZE = 400000007ULL;
			pair<unsigned long long, int>* table;
			table = new pair<unsigned long long, int>[TABLE_SIZE];

			for (unsigned long long i = 0; i < TABLE_SIZE; i++)
			{
				table[i].second = 0;
				table[i].first = 0;
			}

			ifstream binGames("cs242-allGames.bin", std::fstream::binary);
			const int bytesPerRead = 17 * 1024;
			char buffer[bytesPerRead];
			int nBytesRead = 0;
			int nGames = 0;
			ZobristChessHash hash;
			hash.setupPieces();

			int offset = 0;
			while (binGames)
			{
				binGames.read(buffer + offset, bytesPerRead - offset);
				if (binGames)
					nBytesRead = bytesPerRead - offset;
				else
					nBytesRead = binGames.gcount();

				int bytesProcessed;
				for (bytesProcessed = 0; bytesProcessed < nBytesRead - 1;
					bytesProcessed++)
				{
					if (buffer[bytesProcessed] < 0)
					{
						++nGames;
						hash.setupPieces();
					}
					else
					{

						if (buffer[bytesProcessed] < 0 ||
							buffer[bytesProcessed + 1] < 0)
						{
							++nGames;
						}

						hash.movePiece(buffer[bytesProcessed],
							buffer[++bytesProcessed]);
						unsigned long long hashValue = hash.getHash();
						unsigned long long index = hashValue % TABLE_SIZE;
						table[index].first = hashValue;
						table[index].second = table[index].second + 1;
					}
				}

				if (bytesProcessed < bytesPerRead)
				{
					buffer[0] = buffer[bytesProcessed];
					offset = 1;
				}
				else
				{
					offset = 0;
				}
			}
			binGames.close();
			delete[] table;
		}

		TEST_METHOD(testFEN)
		{
			ZobristChessHash hash;
			hash.setupPieces();
			string initialPosition = hash.getPositionInFEN();
			Assert::AreEqual(
				string("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR"),
				initialPosition);
		}

		TEST_METHOD(readOneGame)
		{

			ZobristReader reader("cs242-allGames.bin");

			ZobristChessHash hash;
			char sourceSquare;
			ofstream oneGame("oneGame.fen");

			bool done = false;
			while (!done)
			{
				reader.nextByte(sourceSquare);
				if (sourceSquare < 0)
				{
					done = true;
				}
				else
				{
					char destinationSquare;
					if (reader.nextByte(destinationSquare))
					{
						if (destinationSquare >= 0 && sourceSquare >= 0)
						{
							hash.movePiece(sourceSquare, destinationSquare);
							unsigned long long hashValue = hash.getHash();
							oneGame << hash.getPositionInFEN() << " w KQkq - 0 1" << endl;
						}
					}
				}
			}
				oneGame.close();
		}


		TEST_METHOD(makeHashttable)
		{
			const int nTop = 120;
			int top[nTop] = { 0 };
			string oldGame[nTop];
			string newGame[nTop];

			const unsigned long long TABLE_SIZE = 400000007ULL;
			pair<unsigned long long, int>* table;
			table = new pair<unsigned long long, int>[TABLE_SIZE];

			for (unsigned long long i = 0; i < TABLE_SIZE; i++)
			{
				table[i].second = 0;
				table[i].first = 0;
			}
			//long long* fake = new long long[400000007L];

			ZobristReader reader("cs242-allGames.bin");
			int nGames = 1;
			int ply = 0;
			ZobristChessHash hash;

			char sourceSquare;
			while (reader.nextByte(sourceSquare))
			{
				if (sourceSquare < 0)
				{
					++nGames;
					ply = 0;
					hash.setupPieces();
				}
				else
				{
					char destinationSquare;
					if (reader.nextByte(destinationSquare))
					{
						if (destinationSquare >= 0 && sourceSquare >= 0)
						{
							hash.movePiece(sourceSquare, destinationSquare);
							unsigned long long hashValue = hash.getHash();
							unsigned long long index = hashValue % TABLE_SIZE;


							while (table[index].first != hashValue &&
								table[index].first != 0)
							{
								++index;
								if (index == TABLE_SIZE) index = 0;
							}

							table[index].first = hashValue;
							table[index].second = table[index].second + 1;

							if (ply < nTop)
							{
								if (top[ply] < table[index].second)
								{
									top[ply] = table[index].second;
									oldGame[ply] = newGame[ply];
									newGame[ply] = hash.getPositionInFEN()
										+ " w KQkq - 0 1";
								}
							}
							++ply;
						}
						else
						{
							Assert::Fail();
						}
					}
					else
					{
						Assert::Fail();
					}
				}
			}
			ofstream  fenFile("fenFile.fen");
			for (int i = 0; i < nTop; i++)
			{
				fenFile << newGame[i] << endl;
			}
			fenFile.close();
			cout << top[0] << endl;
		}


		TEST_METHOD(gameReaderReadAllGames)
		{
			GameReader gameReader("cs242-allGames.pgn");
			int sourceSquare;
			int destinationSquare;
			GameReader::GameResult gameProgress;

			bool isWhitesMove = true;
			int plyNumber = 0;
			int castles = 0;
			int nGames = 0;

			bool goodRead = true;
			while (goodRead)
			{
				do
				{
					Assert::IsTrue(isWhitesMove == gameReader.isWhitesMove());

					if (goodRead = gameReader.read(sourceSquare, destinationSquare,
						gameProgress))
					{

						if (!gameReader.isCastling())
						{
							isWhitesMove = !isWhitesMove;
							if (gameProgress == GameReader::GameResult::InProgress)
								++plyNumber;
						}
						else
						{
							++castles;
						}
					}
				} while (goodRead && gameProgress == GameReader::GameResult::InProgress);
				++nGames;
			}

			Assert::IsTrue(2 >= castles);
			//Assert::AreEqual(10, nGames); supposed to fail
		}
	};
}