// .
//

#include "stdafx.h"
#include <stdlib.h>
#include <string>
#include <iostream>
using namespace std;

#include "ZobristChessHash.h"
#include "../zobristLib/GameReader.h"
namespace cs242
{


	ZobristChessHash::ZobristChessHash(unsigned int seed)
	{

		size_t numOfBites = 64; // number of bits needed
		size_t randSize = 15;   // size of the random unit
		size_t size = numOfBites / randSize; // get the actual size
		srand(seed);

		for (size_t i = 0; i < (SquareOccupant::N_PIECE_TYPES); i++)  // for all piece types
		{
			for (size_t j = 0; j < SquareOccupant::N_SQUARES; j++)  // for squares
			{
				unsigned long long hashValue = 0;
				for (size_t k = 0; k < size; k++)
				{   // get k of size rand values
					hashValue = rand() | hashValue;
					if (k != (size - 1))
					{
						// if not equal shift
						hashValue = hashValue << 15;
					}
				}
				// shift 4
				hashValue = hashValue << 4;
				hashValue = (rand() & 15) | hashValue;
				// add to mask
				masks_[i][j] = hashValue;
			}
		}
		// clear the board
		clearBoard();
	}

	unsigned long long ZobristChessHash::clearBoard()
	{

		for (size_t i = 0; i < (SquareOccupant::N_SQUARES); i++)
		{

			hashValue_ = hashValue_ ^ board_[i];
			hashValue_ = hashValue_ = masks_[SquareOccupant::EMPTY_SQUARE][i];
			board_[i] = masks_[SquareOccupant::EMPTY_SQUARE][i];
		}
		return hashValue_;
	}

	ZobristChessHash::SquareOccupant ZobristChessHash::getPieceIndex(char symbol, bool isWhite)
	{
		enum PIECELETTERS { E = ' ', P = 'P', N = 'N', B = 'B', R = 'R', Q = 'Q', K = 'K' };
		int retval;

		switch (symbol)
		{
		case PIECELETTERS::E: retval = SquareOccupant::EMPTY_SQUARE;
			break;
		case PIECELETTERS::P: retval = SquareOccupant::WHITE_PAWN;
			break;
		case PIECELETTERS::N: retval = SquareOccupant::WHITE_KNIGHT;
			break;
		case PIECELETTERS::B: retval = SquareOccupant::WHITE_BISHOP;
			break;
		case PIECELETTERS::R: retval = SquareOccupant::WHITE_ROOK;
			break;
		case PIECELETTERS::Q: retval = SquareOccupant::WHITE_QUEEN;
			break;
		case PIECELETTERS::K: retval = SquareOccupant::WHITE_KING;
			break;
		default: retval = SquareOccupant::EMPTY_SQUARE;
			break;
		}

		if (!isWhite && retval != SquareOccupant::EMPTY_SQUARE)
		{

			int offset = SquareOccupant::BLACK_OFFSET;
			retval = retval + offset;
		}

		return (SquareOccupant)retval;
	}


	int ZobristChessHash::getSquareIndex(const std::string& squareName)
	{

		return GameReader::squareNameToInt(squareName);


	}

	unsigned long long ZobristChessHash::placePiece(char piece, bool isWhite, const std::string& squareName)
	{
		int square = getSquareIndex(squareName);
		removePiece(squareName, square);

		ZobristChessHash::SquareOccupant item = getPieceIndex(piece, isWhite);
		// update the hash
		hashValue_ = hashValue_ ^ masks_[item][square];
		// update the board
		board_[square] = masks_[item][square];
		return hashValue_;
	}

	unsigned long long ZobristChessHash::removePiece(const std::string& squareName, int square)
	{

		// remove old hash value
		hashValue_ = hashValue_ ^ board_[square];
		// return
		return hashValue_;
	}

	unsigned long long ZobristChessHash::movePiece(char piece, bool isWhite,
		const std::string& sourceSquare, const std::string& destinationSquare)
	{
		// empty dest
		placePiece(getPieceLetter(SquareOccupant::EMPTY_SQUARE), isWhite, sourceSquare);
		// add piece to source
		placePiece(piece, isWhite, destinationSquare);
		return hashValue_;
	}

	unsigned long long ZobristChessHash::movePiece(int sourceSquare, int destinationSquare)
	{

		int i = 0;
		while (board_[sourceSquare] != masks_[i][sourceSquare])
		{
			++i;
		}

		placePiece(getPieceLetter(SquareOccupant::EMPTY_SQUARE), true, GameReader::intToSquareName(sourceSquare));
		placePiece(getPieceLetter((SquareOccupant)i), isWhite((SquareOccupant)i), GameReader::intToSquareName(destinationSquare));

		return hashValue_;
	}

	bool  ZobristChessHash::isWhite(SquareOccupant piece)
	{
		return  (piece <= BLACK_OFFSET);
	}



	string  ZobristChessHash::getPositionInFEN()
	{
		// num of rowas and colums
		const int N_ROWS = 8;
		const int N_COLUMNS = 8;
		string rowPosition[N_ROWS];

		// create a tempary board
		char tmpBoard[N_ROWS][N_COLUMNS];

		int nextSquare = 0;

		// while not all colums 
		for (int column = 0; column < N_COLUMNS; column++)
		{
			// for loop rows
			for (int row = 0; row < N_ROWS; row++)
			{

				// add piece to tempboard
				tmpBoard[row][column] = getPieceLetter(piecePosition_[nextSquare]);

				++nextSquare;
			}
		}

		for (int row = 0; row < N_ROWS; ++row)
		{

			// repeat for rows
			int nEmptySquares = 0;
			for (int column = 0; column < N_COLUMNS; ++column)
			{
				char letter = tmpBoard[row][column];




				if (letter == ' ')
				{
					++nEmptySquares;
				}
				else
				{



					if (nEmptySquares > 0)
					{
						rowPosition[row] += (char)('0' + nEmptySquares);
						nEmptySquares = 0;
					}

					rowPosition[row] += letter;
				}
			}
			if (nEmptySquares > 0)
			{
				rowPosition[row] += (char)('0' + nEmptySquares);
			}
		}


		string result;

		// Write top 7 rows
		for (int row = N_ROWS - 1; row >= 0; --row)
		{
			result = result + rowPosition[row];
			if (row != 0) result += "/";
		}

		return  result;
	}



	unsigned long long ZobristChessHash::setupPieces()
	{
		// White Pieces

		// Pawns
		placePiece('P', true, "a2");
		placePiece('P', true, "b2");
		placePiece('P', true, "c2");
		placePiece('P', true, "d2");
		placePiece('P', true, "e2");
		placePiece('P', true, "f2");
		placePiece('P', true, "g2");
		placePiece('P', true, "h2");


		// Rooks
		placePiece('R', true, "a1");
		placePiece('R', true, "h1");



		// Knights
		placePiece('N', true, "b1");
		placePiece('N', true, "g1");

		// Bishops
		placePiece('B', true, "c1");
		placePiece('B', true, "f1");

		// Queen and King
		placePiece('Q', true, "d1");
		placePiece('K', true, "e1");

		// Black Pieces



		// Pawns
		placePiece('P', false, "a7");
		placePiece('P', false, "b7");
		placePiece('P', false, "c7");
		placePiece('P', false, "d7");
		placePiece('P', false, "e7");
		placePiece('P', false, "f7");
		placePiece('P', false, "g7");
		placePiece('P', false, "h7");

		// Rooks
		placePiece('R', false, "a8");
		placePiece('R', false, "h8");



		// Knights
		placePiece('N', false, "b8");
		placePiece('N', false, "g8");



		// Bishops
		placePiece('B', false, "c8");
		placePiece('B', false, "f8");


		// Queen and King
		placePiece('Q', false, "d8");
		placePiece('K', false, "e8");

		return hashValue_;
	}

	unsigned long long ZobristChessHash::getHash() const
	{
		return hashValue_;
	}



	char ZobristChessHash::getPieceLetter(SquareOccupant piece)
	{
		char symbol = ' ';
		switch (piece)
		{
		case EMPTY_SQUARE:
			symbol = ' ';
			break;

		case WHITE_PAWN:
			symbol = 'P';
			break;

		case WHITE_KNIGHT:
			symbol = 'N';
			break;

		case WHITE_BISHOP:
			symbol = 'B';
			break;

		case WHITE_ROOK:
			symbol = 'R';
			break;

		case WHITE_QUEEN:
			symbol = 'Q';
			break;

		case WHITE_KING:
			symbol = 'K';
			break;

		case BLACK_PAWN:
			symbol = 'p';
			break;

		case BLACK_KNIGHT:
			symbol = 'n';
			break;

		case BLACK_BISHOP:
			symbol = 'b';
			break;

		case BLACK_ROOK:
			symbol = 'r';
			break;

		case BLACK_QUEEN:
			symbol = 'q';
			break;

		case BLACK_KING:
			symbol = 'k';
			break;
		}

		return symbol;
	}

}
