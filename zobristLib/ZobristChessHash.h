#pragma once
#include <string>

namespace cs242
{
	class ZobristChessHash
	{
	public:
		enum SquareOccupant{
			EMPTY_SQUARE,
			WHITE_PAWN, WHITE_KNIGHT, WHITE_BISHOP, WHITE_ROOK, WHITE_QUEEN, WHITE_KING,
			BLACK_PAWN, BLACK_KNIGHT, BLACK_BISHOP, BLACK_ROOK, BLACK_QUEEN, BLACK_KING,
			N_PIECE_TYPES, N_SQUARES = 64, BLACK_OFFSET = 6
		};

		ZobristChessHash(unsigned int seed = 0x23425123);

		static SquareOccupant getPieceIndex(char symbol, bool isWhite);
	
		static char getPieceLetter(SquareOccupant piece);
		static int getSquareIndex(const std::string& squareName);

		unsigned long long clearBoard();

		unsigned long long placePiece(char piece, bool isWhite, const std::string& squareName);
		unsigned long long removePiece(const std::string& squareName, int square);

		unsigned long long position_[N_SQUARES];

		unsigned long long getHash() const;

		unsigned long long movePiece(int sourceSquare, int destinationSquare);

		bool isWhite(SquareOccupant piece);

		unsigned long long movePiece(char piece, bool isWhite,
			const std::string& sourceSquare, const std::string& destinationSquare);

		unsigned long long setupPieces();
		std::string getPositionInFEN();// const;

	private:
		unsigned long long masks_[N_PIECE_TYPES][N_SQUARES];
		unsigned long long hashValue_;
		enum SquareOccupant piecePosition_[N_SQUARES];
		unsigned long long board_[N_SQUARES];
	};
}
