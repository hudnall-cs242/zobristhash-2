#include "stdafx.h"
#include <cassert>
using namespace std;
#include "GameReader.h"


namespace cs242
{
	GameReader::GameReader(const string& filename) :
		inputStream_(filename),
		isCastling_(false),
		isWhitesMove_(true),
		whiteCanCastle_(true),
		blackCanCastle_(true)
	{
	}

	int GameReader::convertToBinary(const string& outfile)
	{
		int sourceSquare;
		int destinationSquare;
		GameReader::GameResult gameProgress;
		ofstream out(outfile.c_str(), std::fstream::binary);

		bool isWhitesMove = true;
		int nGames = 0;
		char thingToWrite[2];

		bool goodRead = true;
		while (goodRead)
		{
			do
			{
				if (goodRead = read(sourceSquare, destinationSquare,
					gameProgress))
				{
					if (gameProgress == GameResult::InProgress)
					{
						thingToWrite[0] = sourceSquare;
						thingToWrite[1] = destinationSquare;
						assert(thingToWrite[0] < 64);
						assert(thingToWrite[1] < 64);
						out.write(thingToWrite, 2);
					}
					else
					{
						thingToWrite[0] = -gameProgress;
						out.write(thingToWrite, 1);
						assert(thingToWrite[0] < 64);
					}
				}
			} while (goodRead && gameProgress == GameReader::GameResult::InProgress);
			if (goodRead) ++nGames;
		}
		out.close();
		return nGames;
	}

	bool GameReader::isWhitesMove() const { return isWhitesMove_; }
	bool GameReader::isCastling() const { return isCastling_; }

	int GameReader::squareNameToInt(const std::string& squareName)
	{
		return (squareName[0] - 'a') *
			SQUARES_PER_COLUMN + squareName[1] - '1';
	}

	string GameReader::intToSquareName(int squareNumber)
	{
		string result("xx");
		result[0] = 'a' + squareNumber / SQUARES_PER_COLUMN;
		result[1] = '1' + squareNumber % SQUARES_PER_COLUMN;
		return result;
	}


	bool GameReader::parseResult(const string& theString,
		GameReader::GameResult& result)
	{
		const int MIN_RESULT_LENGTH = 3;
		bool parses = true;
		if (theString.length() >= MIN_RESULT_LENGTH)
		{
			if (theString[0] == '0')
			{
				result = GameReader::BlackWins;
			}
			else
			{
				if (theString[0] == '1')
				{
					if (theString[1] == '-')
						result = GameReader::WhiteWins;
					else
						result = GameReader::Draw;
				}
				else
				{
					parses = false;
				}
			}
		}
		return parses;
	}

	bool GameReader::parseMove(const string& theString, int& source, int& destination)
	{
		const int MIN_MOVE_LENGTH = 5;

		// If it is a move with a dash
		bool parses = (theString.length() >= MIN_MOVE_LENGTH &&
			theString[0] >= 'a' && theString[0] <= 'h' &&
			theString[1] >= '1' && theString[1] <= '8' &&
			theString[2] == '-' &&
			theString[3] >= 'a' && theString[3] <= 'h' &&
			theString[4] >= '1' && theString[4] <= '8');

		if (parses)
		{
			source = this->squareNameToInt(theString.substr(0, 2));
			destination = this->squareNameToInt(theString.substr(3, 2));
		}
		else
		{
			// If it is a move without a dash
			parses = theString[0] >= 'a' && theString[0] <= 'h' &&
				theString[1] >= '1' && theString[1] <= '8' &&
				theString[2] >= 'a' && theString[2] <= 'h' &&
				theString[3] >= '1' && theString[3] <= '8';

				if (parses)
				{
					source = squareNameToInt(theString.substr(0, 2));
					destination = squareNameToInt(theString.substr(2, 2));
				}
		}

		return parses;
	}

	bool GameReader::read(int& sourceSquare, int& destinationSquare,
		GameResult& result)
	{
		string next;
		bool parses = true;
		bool returnValue = true;
		if (isCastling_)
		{
			sourceSquare = castlingSource_;
			destinationSquare = castlingDestination_;
			isCastling_ = false;
		}
		else
		{
			try
			{
				if (inputStream_ >> next)
				{
					result = GameResult::InProgress;
					if (!parseMove(next, sourceSquare, destinationSquare))
					{
						if (!parseResult(next, result))
						{
							parses = false;
						}
					}
					else
					{
						// parsed a move 
						assert(sourceSquare >= 0 && sourceSquare < 64);
						assert(destinationSquare >= 0 && destinationSquare < 64);

						if (isWhitesMove_)
						{
							checkWhiteCastle(sourceSquare, destinationSquare);
						}
						else
						{
							checkBlackCastle(sourceSquare, destinationSquare);
						}
					}
				}
				else
				{
					parses = false;
				}
			}
			catch (...)
			{
				parses = false;
			}
		}
		isWhitesMove_ = !isWhitesMove_;
		return parses;
	}


	void GameReader::checkBlackCastle(int& sourceSquare, int& destinationSquare)
	{
		if (blackCanCastle_)
		{
			int blackKingSquare = GameReader::squareNameToInt("e8");
			if (sourceSquare == blackKingSquare)
			{
				// Black is moving the king - can no longer castle.
				blackCanCastle_ = false;

				int kingSideDestination = GameReader::squareNameToInt("g8");
				if (destinationSquare == kingSideDestination)
				{
					// Start castling king-side
					isCastling_ = true;
					castlingSource_ =
						GameReader::squareNameToInt("h8");
					castlingDestination_ =
						GameReader::squareNameToInt("f8");
					isWhitesMove_ = !isWhitesMove_;
				}

				int queenSideDestination = GameReader::squareNameToInt("c8");
				if (destinationSquare == queenSideDestination)
				{
					// Start castling queen-side
					isCastling_ = true;
					castlingSource_ =
						GameReader::squareNameToInt("a8");
					castlingDestination_ =
						GameReader::squareNameToInt("d8");
					isWhitesMove_ = !isWhitesMove_;
				}
			}
		}
	}

	void GameReader::checkWhiteCastle(int& sourceSquare, int& destinationSquare)
	{
		if (whiteCanCastle_)
		{
			int whiteKingSquare = GameReader::squareNameToInt("e1");
			if (sourceSquare == whiteKingSquare)
			{
				// White is moving the king - can no longer castle.
				whiteCanCastle_ = false;

				int kingSideDestination = GameReader::squareNameToInt("g1");
				if (destinationSquare == kingSideDestination)
				{
					// Start castling king-side
					isCastling_ = true;
					castlingSource_ =
						GameReader::squareNameToInt("h1");
					castlingDestination_ =
						GameReader::squareNameToInt("f1");
					isWhitesMove_ = !isWhitesMove_;
				}

				int queenSideDestination = GameReader::squareNameToInt("c1");
				if (destinationSquare == queenSideDestination)
				{
					// Start castling queen-side
					isCastling_ = true;
					castlingSource_ =
						GameReader::squareNameToInt("a1");
					castlingDestination_ =
						GameReader::squareNameToInt("d1");
					isWhitesMove_ = !isWhitesMove_;
				}
			}
		}
	}

	GameReader::~GameReader()
	{
		inputStream_.close();
	}
}