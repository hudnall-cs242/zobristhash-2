#pragma once
#include <string>
#include <iostream>
#include <fstream>

namespace cs242
{
	class GameReader
	{
	public:
		enum { SQUARES_PER_COLUMN = 8 };
		enum GameResult {InProgress, Draw, WhiteWins, BlackWins};
		GameReader(const std::string& filename);

		static int squareNameToInt(const std::string& squareName);
		static std::string intToSquareName(int squareNumber);

		int convertToBinary(const std::string& outfile);

		bool read(int& sourceSquare, int& destinationSquare, 
			GameResult& result);

		void checkWhiteCastle(int& sourceSquare, int& destinationSquare);
		void checkBlackCastle(int& sourceSquare, int& destinationSquare);
		
		bool isWhitesMove() const;
		bool isCastling() const;


		virtual ~GameReader();

	private:
		bool parseMove(const std::string& theString, int& source, int& destination);
		bool parseResult(const std::string& theString, GameResult& result);

		bool isCastling_;
		bool isWhitesMove_;
		bool whiteCanCastle_;
		bool blackCanCastle_;
		int castlingSource_;
		int castlingDestination_;
		std::ifstream inputStream_;
	};
}
